<?php

namespace App;

use Illuminate\Database\Eloquent\Model;



class Partner extends Model
{

    public static function getAll(){
        return Partner::orderBy("sort", "ASC")->get();
    }

    public static function getAllOnMain(){
        return Partner::where('in_main', 1)->orderBy("sort", "ASC")->get();
    }

}

