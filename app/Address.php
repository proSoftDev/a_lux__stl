<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;


class Address extends Model
{
    use Translatable;
    protected $translatable = ['name'];

    public static function getContent(){
        return Address::first();
    }

    public static function search($text){
        if(app()->getLocale() == 'ru') $model = self::where('name', 'LIKE', "%{$text}%")->first();
        else $model = self::whereTranslation('name', 'LIKE', "%{$text}%", [app()->getLocale()], false)->first();
        return $model;
    }
    
}
