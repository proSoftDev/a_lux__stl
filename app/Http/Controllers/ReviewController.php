<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 02.03.2020
 * Time: 18:02
 */

namespace App\Http\Controllers;


use App\AboutReview;

class ReviewController extends Controller
{


    public function index(){

        $count = AboutReview::getCount($this->paginationPerPage);
        $model = AboutReview::getAllWithPagination($this->paginationPerPage);
        if($model->count() == 0) abort('404');

        return view('reviews.index', compact('model', 'count'));
    }



}
