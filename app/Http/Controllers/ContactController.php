<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 18.02.2020
 * Time: 16:56
 */

namespace App\Http\Controllers;


use App\Address;
use App\Branch;
use App\Helpers\TranslatesCollection;

class ContactController extends Controller
{
    public function index(){

        $branches = Branch::getAll();
        TranslatesCollection::translate($branches, app()->getLocale());

        return view('contact.index', compact('address', 'branches'));
    }

}
