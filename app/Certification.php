<?php

namespace App;

use Illuminate\Database\Eloquent\Model;



class Certification extends Model
{

    public static function getAll(){
        return Certification::orderBy("sort", "ASC")->get();
    }
    
}
