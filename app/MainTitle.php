<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;


class MainTitle extends Model
{
    use Translatable;
    protected $translatable = ['text'];

    public static function getAll(){
        return MainTitle::get();
    }


}
