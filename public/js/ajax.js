$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});



function showCalculateForm() {
    if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
        $(".burger-menu-btn").click()
    };

}

function setLocation(url) {
    window.location.href = url;
}




$('body').on('click','#serviceRequestBtn', function(){

    Swal.showLoading();
    $.ajax({
        type: 'POST',
        url: '/service-request',
        data: $(this).closest('form').serialize(),
        dataType: 'json',
        success: function (response) {
            Swal.close();
            if(response.status == 1){
            dataLayer.push({'event': 'formsend'});
                Swal.fire(response.title, response.content, 'success');
                $("#counter")[0].reset();
            }else{
                Swal.fire({
                    icon: 'error',
                    text: response.error
                });
            }
        },
        error: function () {
            Swal.close();
            Swal.fire({
                icon: 'error',
                text: 'Что-то пошло не так!'
            });
        }
    });
});





$('body').on('click','#consultationBtn', function(){

    Swal.showLoading();
    $.ajax({
        type: 'POST',
        url: '/consultation-request',
        data: $(this).closest('form').serialize(),
        dataType: 'json',
        success: function (response) {
            Swal.close();
            if(response.status == 1){
                Swal.fire(response.title, response.content, 'success');
                $("#consultationModal form")[0].reset();
                dataLayer.push({'event': 'formsend'});
            }else{
                Swal.fire({
                    icon: 'error',
                    text: response.error
                });
            }
        },
        error: function () {
            Swal.close();
            Swal.fire({
                icon: 'error',
                text: 'Что-то пошло не так!'
            });
        }
    });
});




$('body').on('click','#feedbackBtn', function(){

    Swal.showLoading();
    $.ajax({
        type: 'POST',
        url: '/feedback-request',
        data: $(this).closest('form').serialize(),
        dataType: 'json',
        success: function (response) {
           
            Swal.close();
            if(response.status == 1){
                Swal.fire(response.title, response.content, 'success');
                $("#feedbackModal form")[0].reset();
                dataLayer.push({'event': 'formsend'});
            }else{
                Swal.fire({
                    icon: 'error',
                    text: response.error
                });
            }
        },
        error: function () {
            Swal.close();
            Swal.fire({
                icon: 'error',
                text: 'Что-то пошло не так!'
            });
        }
    });
});


