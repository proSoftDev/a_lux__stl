@extends('layouts.app')
@section('content')

    @include('/partials/breadcrumbs')
    <div class="contact-page">
        <div class="contact-image">
            <img src="{{ asset('images/service6.png') }}" alt="">
        </div>
        <div class="maps">
        <iframe src="{{ setting('site.iframe_url') }}" width="100%" height="650" frameborder="0"></iframe>
        </div>
        <div class="maps-modal" id="contacts">
            <h1>STL LOGISTICS</h1>
            <div class="maps-modal-link">
                <img src='{{ asset('images/maps1.png') }}'>
                <p>{{ $address->name }}</p>
            </div>

            <div class="maps-modal-link">
                <img src='{{ asset('images/maps2.png') }}'>
                <a href="mailto:{{ setting('site.email') }}">
                    <p class="email-p">{{ setting('site.email') }} </p>
                </a>
            </div>
            <div class="maps-modal-link">
                <img src='{{ asset('images/maps3.png') }}'>
                <div class="contact-info">
                    <p>@lang('texts.Мобильный') </p>
                    <a href="tel:{{ setting('site.telephone') }}">
                        <span>{{ setting('site.telephone') }}</span>
                    </a>
                    <p>@lang('texts.Мобильный') </p>
                    <a href="tel:{{ setting('site.telephone2') }}">
                        <span>{{ setting('site.telephone2') }}
                        <img src="{{ asset('images/whatsapp.png') }}" alt=""> <img src="{{ asset('images/viber.png') }}" alt=""></span>
                    </a>
                </div>
            </div>
            <div class="maps-modal-link">
                <button data-toggle="modal" data-target="#feedbackModal">@lang('buttons.Заказать обратный звонок')</button>
            </div>
        </div>
    </div>

    <div class="company-branches" id="filials">
        <div class="container">
            <h1>@lang('texts.Филиалы компании')</h1>
            <div class="branch-inner">
                <div class="row">
                    @php $m = 0 @endphp
                    @foreach($branches as $k => $v)
                        @php $m++ @endphp
                        <div class="col-xl-4 col-md-6 col-12 contact-border"   id="filial_{{ $v->id }}">
                            <div class="branch-links {{ $m % 3 == 2 ? 'branch-item br-content':''}}
                            {{ $m % 3 == 1 ? 'branch-item':''}} {{ $m % 3 == 0 ? 'br-content':''}}">
                                <div class="branch-image">
                                    <img src="{{ Voyager::image($v->icon) }}" alt="">
                                </div>
                                <div class="branch-content">
                                    <h2>{{ $v->country }}</h2>
                                    <p>{{ $v->address }}</p>
                                    @if($v->tel1 != null)
                                        <p>{{ $v->tel1_status == 0 ? "tel":"fax"  }}: <b> {{ $v->tel1 }}</b></p>
                                    @endif
                                    @if($v->tel2 != null)
                                        <p>{{ $v->tel2_status == 0 ? "tel":"fax"  }}: <b>  {{ $v->tel2 }}</b></p>
                                    @endif
                                    <p>e-mail: <b>{{ $v->email }}</b></p>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

@endsection
