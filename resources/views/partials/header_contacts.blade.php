<li class="nav-item phone">
    <a class="nav-link top-link" href="tel:{{ setting('site.telephone') }}">
        <img src="{{ asset('icons/phone%2018px.svg') }}">{{ setting('site.telephone') }}</a>
</li>
<li>
	<a class="nav-link top-link" href="https://wa.me/{{ setting('site.whatsapp') }}">
        <img src="{{ asset('icons/Whatssap2020.svg') }}">+{{ setting('site.whatsapp') }}
	</a>
</li>
<li class="nav-item email">
    <a class="nav-link top-link" href="mailto:{{ setting('site.email') }}"><img src="{{ asset('icons/Group.png') }}"> {{ setting('site.email') }}</a>
</li>
{{--<li class="nav-item skype">--}}
    {{--<a class="nav-link top-link" href="#"> <img src="{{ asset('icons/skype%2018px.svg') }}"> {{ setting('site.skype') }}</a>--}}
{{--</li>--}}
