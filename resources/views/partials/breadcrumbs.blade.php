@if($page->getPage()->name != null)
    <div class="container">
        <div class="crumb-top">
            <nav aria-label="breadcrumb ">
                <ol class="breadcrumb crumbs">
                    <li class="breadcrumb-item link-active"><a href="{{ route('home') }}">@lang('texts.Главная')</a></li>
                    <li class="breadcrumb-item link-active"><a href="{{ route('all_service') }}">@lang('texts.Услуги')</a></li>
                    <li class="breadcrumb-item active" aria-current="page">
                       {{ $page->getPage()->translate(app()->getLocale())->name}}</li>
                </ol>
            </nav>
        </div>
    </div>
@else
    <div class="container">
        <div class="crumb-top">
            <nav aria-label="breadcrumb ">
                <ol class="breadcrumb crumbs">
                    <li class="breadcrumb-item link-active"><a href="{{ route('home') }}">@lang('texts.Главная')</a></li>
                    <li class="breadcrumb-item active" aria-current="page">
                        {{ $page->getPage()->translate(app()->getLocale())->title}}
                    </li>
                </ol>
            </nav>
        </div>
    </div>

    <div class="title">
        <div class="container">
            <h1>{{ $page->getPage()->translate(app()->getLocale())->title}}</h1>
        </div>
    </div>
@endif

