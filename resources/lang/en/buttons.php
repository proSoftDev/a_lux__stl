<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'Получить консультацию' => 'Get consultation',
    'Скачать брошюру' => 'Download brochure',
    'Больше отзывов о нас' => 'More reviews about us',
    'Заказать обратный звонок' => 'Request a call back',
    'Посмотреть еще' => 'View more',
    'РАСЧИТАТЬ СТОИМОСТЬ' => 'CALCULATE COST',
    'Рассчитать' => 'Calculate',
    'презентация' => 'presentation',
    'Назад' => 'Previous',
    'Вперед' => 'Next',
    'Рассчитать стоимость' => 'Calculate cost',
    'Читать далее' => 'Read more',
    'Вернуться на главную' => 'Go back',



];
